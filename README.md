Pipeline Demo App

Pipeline contains 3 stages:
1. Build project
2. Run unit tests
3. Build Docker image and push it to Docker registry(Docker Hub)

Environment variables to set:
1. REGISTRY_USER
2. REGISTRY_PASS
3. IMAGE_NAME
4. IMAGE_TAG
