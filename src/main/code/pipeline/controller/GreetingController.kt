package pipeline.controller

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/greeting")
class GreetingController {

    @GetMapping("/{name}")
    fun getGreeting(@PathVariable("name") name: String): ResponseEntity<Response> =
        ResponseEntity.ok(Response("Hello, $name"))
}

data class Response(val greeting: String)