package controller

import pipeline.controller.GreetingController
import pipeline.controller.Response
import org.amshove.kluent.`should be equal to`
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

class GreetingControllerTest {

    companion object {
        private const val NAME = "FANTA"
    }

    private val testInstance = GreetingController()

    @Test
    fun `should return greetings with OK status`() {
        val actualResult = testInstance.getGreeting(NAME)

        actualResult `should be equal to` ResponseEntity.ok(Response("Hello, FANTA"))
        actualResult.statusCode `should be equal to` HttpStatus.OK
    }
}